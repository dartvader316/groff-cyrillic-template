
TROFF_BIN= groff

SRC = main.tr
GROFF_FLAGS = -egp -F "${shell pwd}/fonts" -K "utf-8" -mom

all: pdf

pdf: $(SRC)
	$(TROFF_BIN) $(GROFF_FLAGS) -Tpdf  $< > out.pdf
ps: $(SRC)
	$(TROFF_BIN) $(GROFF_FLAGS) -Tps  $< > out.ps
utf8: $(SRC)
	$(TROFF_BIN) $(GROFF_FLAGS) -Tutf8  $< > out.utf8

run: pdf
	xdg-open out.pdf

clean:
	rm -f out.pdf out.ps out.utf8
